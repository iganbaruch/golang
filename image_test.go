package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestGetAsciiImage(t *testing.T) {

	fmt.Println("Testing GetAsciiImage")

	// Fake the image string (loading and converting an image fails for some reason in the test scope)
	res := GetAsciiImage("🦊", helloFrom)

	// Check if AI is included
	tables := []string{"AI-powered"}

	for _, table := range tables {
		fmt.Printf("Checking GetAsciiImage for '%s'.", table)
		if strings.Contains(res, table) != true {
			t.Errorf("GetAsciiImage failed at checking for '%s'.", table)
		}
	}
}
